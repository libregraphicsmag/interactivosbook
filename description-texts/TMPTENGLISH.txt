Title: Open Source Digital Pattern Making

In a technology-driven world, access to digital tools which are modifiable and customizable is becoming more and more essential. The Tau Meta Tau Physica project for open source pattern design aims to create a tool which enables designers, patternmakers, and consumers to express ideas, customize products, order individually-sized unique patterns and garments, and enter into creative business relationships.

The beneficiaries of this pattern making design tool are consumers, independent designers and patternmakers, small and regional garment manufacturers and sewists, and other persons and businesses for whom commercially available proprietary design software has been an inappropriate or marginal fit.

Collaborators in the Interactivos?'13 workshop re-defined the current architecture, software engine, user interface, and project roadmap and began development of an HTML5-compliant web-based application. Many of the new HTML5 specifications regarding data storage are implemented incompletely in some browsers, so much effort was spent in finding a path through the conflicting issues, resulting in the creation of a web-application which can also run locally without internet connectivity. A second result of the workshop is the draft specification for a Human Definition File format (*.hdf).  This file format fills an existing need in the garment industry for specifying an individual's body measurements in a non-proprietary open data format. The third result of the workshop is the development of generating a customizable pattern using Processing.  This methodology holds promise for becoming a teaching tool for math skill development and for learning pattern design.

This workshop was extremely successful in contributing new tools and ideas to the open source community.

