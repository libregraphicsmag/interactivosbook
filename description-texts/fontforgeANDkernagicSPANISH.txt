Colaboración en tiempo real con FontForge
=================================================
Este proyecto extiende la colaboración tipográfica en tiempo real de FontForge a la web y abre un debate con los diseñadores de fuentes, escritores técnicos y programadores sobre el rumbo de la colaboración de fuentes libres.

Es común para diseñadores trabajar juntos en pequeños grupos de fuentes tipográficas e intercambiar archivos.  El proceso de sincronización de la versión principal y el registro de versiones anteriores se puede lograr a través de correo electrónico y la opción de “Salvar Como”, o tal vez con sistemas de control de versiones.  Mientras que este sistema de trabajo es ideal para procesos lentos  de diseño, la colaboración en tiempo real, permite un progreso rápido y un cambio en la calidad del proceso de diseño.

FontForge es un editor de fuentes libres, disponible bajo una licencia GNU GPL v3 o superior.  Al igual que un juego en la red de múltiples participantes, un usuario puede ahora ejecutar FontForge como 'anfitrión' y otros usuarios pueden conectarse a él, y editar esa fuente tipográfica al mismo tiempo.  Cuando un usuario edita un glifo, sus modificaciones son enviadas a todas las copias de los usuarios que están colaborando.  Esta característica fue iniciada por Dave Crossland e implementada por el Dr. Ben Martin mediante el uso de la librería ZeroMQ (zguide.zeromq.org) antes del taller.

Las fuentes se diseñan mediante un proceso iterativo, donde se realizan cambios a las formas de las letras en un editor de fuentes, para luego revisarlas en uso, y así, tomar más decisiones de diseño.  Un editor de fuentes como FontForge no es la única aplicación utilizada para diseñar fuentes, ya que es necesario mostrar la fuente en composiciones tipográficas. Cada vez es más común utilizar navegadores de Internet para esta tarea.

En Interactivos?'13, Ben, Dave y otros han implementado una manera en que los usuarios pueden ver la fuente desde una sesión colaborativa de FontForge en un navegador de Internet, evidenciando cada uno de los cambios hechos en tiempo real.  La página web permite que los usuarios vean cambios recientes hechos a la fuente en vivo, y comparar con otras fuentes.

Un video mostrando el prototipo inicial se encuentra en youtu.be/rMR7H8IWlTI y un texto documentando en vivo el proyecto en titanpad.com/fontforge-interactivos13.

Kernagic
=============
Kernagic es un programa libre para modificar automáticamente espaciado general y entre dos caracteres.

Descubrir y ajustar el espacio entre glifos con interletraje, es uno de los procesos finales, y que toman más tiempo al dominar el diseño tipográfico. Este proceso ajusta tanto la métrica de la fuente (los espacios normales a la izquierda y derecha de cada glifo, conocido como side-bearings), como también los ajustes para pares específicos de glifos (conocido como el interletraje de pares.)
Kernagic pretende automatizar este proceso. La heurística implementada actualmente, genera un mapa de distancia basado en tramas, para medir las distancias entre los lados opuestos de cualquier par posible de glifos.  Por cada mapa que encuentra, se forma una escala de grises entre las formas de las letras, y genera una tabla de interletrado

En el futuro, el exhaustivo interletrado “óptimo” será usado para generar side-bearings e interletrado basado en clases.  Kernagic es una aplicación independiente con interfaz gráfica que lee y escribe el formato de intercambio UFO (Unified Font Object) de origen de la fuente.  Está escrito en C y usa GTK3, glib y Cairo.  La interfaz gráfica permite a los usuarios afinar interactivamente sus parámetros, y puede también ser ejecutado desde la línea de comando como parte de un flujo de trabajo automatizado.
El código fuente está disponible bajo GNU Affero GPL v3 o superior en github.com/hodefoting/kernagic.ión con otras heurísticas, también.



