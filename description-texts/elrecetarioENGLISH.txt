El-Recetario.net is a collaborative platform for research and experimentation on the use of waste to construct furniture and accessories, where people can share what they do and how they do it, learning from that and working with others.

The project combines four variables:
1. research on the waste we produce and its possible reuse as raw material for new objects;
2. creating manuals that can evolve and be shared;
3. interaction and collaboration amongst community members;
4. and continuous feedback between the virtual platform and actions in real life.

The proposal for the workshop is the update of the existing platform El-Recetario.net to turn it into an open space that allows collaborative work amongst its users to generate shareable materials that encourage DIY and reuse of waste.
